# wizbackup

A minimalist script based on rsync and keeps a history of N days using hard-links. While it's very very simple, `wizbackup` gets the job done and has the added benefit of not requiring any tools to read or restore a backup.

> NOTE: This is a fork of
> * git://codewiz.org/bernie/wizbackup.git


## Installation

```bash
# == List the machines to backup ==

mkdir -p /backup/HOSTS
cat >/backup/HOSTS/example <<__EOF__
host1.example.com
host2.example.com
__EOF__


# == Optionally, exclude files from backups ==

mkdir -p /backup/EXCLUDE
cat >/backup/EXCLUDE/ALWAYS <<__EOF__
/dev/
/mnt/
/proc/
/sys/
/selinux/
__EOF__

cat >/backup/EXCLUDE/host1.example.com <<__EOF__
/var/cache
__EOF__


# == Install wizbackup cronjob ==

cat >/etc/cron.daily/wizbackup <<__EOF__
#!/bin/bash
wizbackup-driver /backup/HOSTS/example /backup
__EOF__


# == Create an ssh keypair for the hosts ==

mkdir -p /etc/wizbackup
ssh-keygen -N '' -c "wizbackup@example.com" -f /etc/wizbackup/ssh_id
ssh-copy-id -f /etc/wizbackup/ssh_id.pub root@host1.example.com
ssh-copy-id -f /etc/wizbackup/ssh_id.pub root@host2.example.com

```
